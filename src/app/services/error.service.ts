
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  errorMsg;

  constructor(private router: Router) {
  }

  findError(error) {
    if (error.error.message != null) {
      switch (error.error.message) {
      case 'INVALID_TOKEN':
        localStorage.removeItem('token');
        this.router.navigate(['/login']);
        break;
      case 'WRONG_TEAM_SIZE':
        this.errorMsg = 'Ogni squadra deve avere lo stesso numero di giocatori';
        return this.errorMsg;
      case 'WRONG_PLAYERS_ERROR':
        this.errorMsg = 'Selezionare tutti i giocatori';
        return this.errorMsg;
      case 'CANNOT_REGISTER_USER':
        // tslint:disable-next-line:max-line-length
        this.errorMsg = 'Sei già iscritto ad un altro evento!';
        return this.errorMsg;
      case 'CANNOT_REMOVE_BINDING':
        this.errorMsg = 'Non puoi annullare la tua iscrizione all\'evento indicato. Contatta uno degli incaricati!';
        return this.errorMsg;
      case 'EMAIL_ALREADY_EXIST':
        this.errorMsg = 'La mail indicata è già stata utilizzata da un altro utente, inseriscine un\'altra e riprova';
        return this.errorMsg;
      default:
        this.errorMsg = 'Qualcosa è andato storto. Riprova!';
        return this.errorMsg;
    }
    }
  }
}
