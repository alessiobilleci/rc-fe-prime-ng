import { Injectable } from '@angular/core';
import { UserDTO } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EventDTO } from '../models/event';
import { ChangePasswordDTO } from '../models/changePassword.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _loggedInUser?: UserDTO;


  constructor(private http: HttpClient) { }
  get loggedInUser(): UserDTO {
    return this._loggedInUser;
  }

  set loggedInUser(user: UserDTO) {
    this._loggedInUser = user;
  }

  register(user: UserDTO) {
    return this.http.post(environment.apiUrl + `/user/register`, user);
  }

  getAll() {
    return this.http.get<UserDTO[]>(`/user/register`);
  }

  getUser() {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.get<UserDTO>(environment.apiUrl + `/user/findInfo`, { headers: header }).pipe(
      map((result) => {
        localStorage.setItem('user', JSON.stringify(result));
        return result;
      })
    );
  }

  changeRole(username, role): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.post<UserDTO>(environment.apiUrl + `/user/changeRole/${username}/${role}`, null, { headers: header });
  }

  delete(username): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.post<UserDTO>(environment.apiUrl + `/user/delete/${username}`, null, { headers: header });
  }

  //Returns all stored users in DB
  getAllUsers() {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.get<UserDTO[]>(environment.apiUrl + `/user/find`, { headers: header }).pipe(
      map((result) => {
        return result;
      })
    );
  }

  bindWithEvent(id) {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.post<EventDTO>(environment.apiUrl + `/user/bindWithEvent/` + id, null, { headers: header });
  }

  recoverPassword(username) {
    return this.http.post<any>(environment.apiUrl + `/user/passwordRecovery/` + username, null)
  }

  changePassword(changePassword) {
    return this.http.post<ChangePasswordDTO>(environment.apiUrl + `/user/changePassword/`, changePassword)
  }

  findSubscribed() {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.get<EventDTO[]>(environment.apiUrl + `/user/findSubscribed/`, { headers: header })
  }

  removeFromEvent(eventId, username) {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.post<any>(environment.apiUrl + `/user/removeFromEvent/` + eventId + '/' + username, null, { headers: header })
  }

  changeNewsletter() {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.post<any>(environment.apiUrl + `/user/changeNewsletterStatus/`, null, { headers: header })
  }

}
