import { Injectable } from '@angular/core';
import { UserDTO } from '../models/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { TokenDTO } from '../models/token';
import { LoginDTO } from '../models/login.model';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  public currentTokenSubject: BehaviorSubject<TokenDTO>;
  public currentToken: Observable<TokenDTO>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentTokenSubject = new BehaviorSubject<TokenDTO>(JSON.parse(localStorage.getItem('token')));
    this.currentToken = this.currentTokenSubject.asObservable();
  }

  login(username, password) {
    return this.http.post<LoginDTO>(environment.apiUrl + `/user/authenticate`, { username, password })
      .pipe(map(loginDTO => {
        if (!loginDTO.token) {
          throw new Error('Username e/o password errati');
        }
        localStorage.setItem('token', JSON.stringify(loginDTO.token));
        localStorage.setItem('role', JSON.stringify(loginDTO.role));
        return loginDTO;
      }));
  }
  public get currentTokenValue(): TokenDTO {
    return this.currentTokenSubject.value;
  }

  logout() {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.post<TokenDTO>(environment.apiUrl + `/user/logout`, null, { headers: header }).pipe(map(token => {
      localStorage.removeItem('token');
      this.currentTokenSubject.next(null);
      this.router.navigate(['/login']);
    }))
  }
}

