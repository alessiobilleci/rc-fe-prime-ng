import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserDTO } from '../models/user';
import { EventDTO } from '../models/event';
import { PlayerDTO } from '../models/player.model';
import {CategoriesDTO} from "../models/categories";

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  getEvents() {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.get<EventDTO[]>(environment.apiUrl + `/event/findActive`, { headers: header }).pipe(
      map((result) => {
        return result;
      })
    );
  }

  getUsers(token) {
    return this.http.get<UserDTO[]>(environment.apiUrl + `/user/find`, token).pipe(
      map((result) => {
        return result;
      })
    );
  }

  createEvent(event): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    const body = event;

    return this.http.post<EventDTO>(environment.apiUrl + `/event/create`, body, { headers: header });
  }

  getCategories(): Observable<any> {
    const header = new HttpHeaders().set(
      'Authorization',
      JSON.parse(localStorage.getItem('token')).token);

    return this.http.post<CategoriesDTO>(environment.apiUrl + `/event/categories`, null, { headers: header });
  }

  editEvent(event): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    const body = event;

    return this.http.post<EventDTO>(environment.apiUrl + `/event/modify`, body, { headers: header });
  }

  findPlayers(id, page) {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.get<PlayerDTO[]>(environment.apiUrl + `/event/findPlayers/` + id + '/' + page, { headers: header });

  }

  setTeam(eventId, blackTeam, whiteTeam) {
    const body = new HttpParams().set("blackTeam", blackTeam).append("whiteTeam", whiteTeam)
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);

    return this.http.post<any>(environment.apiUrl + `/event/setTeam/` + eventId, body, { headers: header });
  }
  deleteEvent(eventId) {
    let header = new HttpHeaders().set(
      "Authorization",
      JSON.parse(localStorage.getItem('token')).token);
    return this.http.post<EventDTO>(environment.apiUrl + `/event/delete/` + eventId, null, { headers: header });
  }
}
