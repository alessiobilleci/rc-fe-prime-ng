import { UserDTO } from './user';

export class EventDTO {
  id?: number;
  category?: string;
  date?: string;
  hiddenTime?: string;
  creator?: UserDTO;
  played: boolean;
  freeSeats?: number;
  totalSeats?: number;
  hourOfFreePeriod?: number;
  hourOfNoDeleteZone?: number;
}
