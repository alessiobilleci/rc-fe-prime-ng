import { TokenDTO } from "./token";

export class LoginDTO {
    token: TokenDTO;
    role: string;
}