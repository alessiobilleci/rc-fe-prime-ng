export class UserDTO {
  public username: string;
  public name: string;
  public surname: string;
  public email: string;
  public role: string;
  public password: string;
  public newsletter: boolean;
  public loggedInUser: UserDTO;
  }

