export class PlayerDTO{
    username? : string;
    name? : string;
    surname? : string;
    team? : string;
    role? : string;
}
