
export class ChangePasswordDTO {
    username?: string;
    currentPassword?: string;
    newPassword?: string;
}