import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { PageNotFoundComponent } from './pages/page_not_found/page-not-found.component';
import { HomeComponent } from './pages/home/home.component';
import { SignUpComponent } from './pages/sign_up/sign-up.component';
import { NewEventComponent } from './pages/new_event/new-event.component';
import { AuthGuard } from './auth.guard';
import { Resolver } from './resolver';

const routes: Routes = [

  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'new-event', component: NewEventComponent },
  { path: 'home', resolve: {posts: Resolver}, canActivate: [AuthGuard], component: HomeComponent, pathMatch: 'full'},
  { path: '', redirectTo:'/login', pathMatch:'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
