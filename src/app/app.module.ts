// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { DialogModule } from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { PasswordModule } from 'primeng/password';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { TooltipModule } from 'primeng/tooltip';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { InputSwitchModule } from 'primeng/inputswitch';
import {InputNumberModule} from 'primeng/inputnumber';
import {AccordionModule} from 'primeng/accordion';



//Components
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { PageNotFoundComponent } from './pages/page_not_found/page-not-found.component';
import { SignUpComponent } from './pages/sign_up/sign-up.component';
import { LoginService } from './services/login.service';
import { UserService } from './services/user.service';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { NewEventComponent } from './pages/new_event/new-event.component';
import { NewIncaricatiComponent } from './pages/new-incaricati/new-incaricati.component';
import { MessageService } from 'primeng/api';
import { HomeComponent } from './pages/home/home.component';
import { ConfirmationService } from 'primeng/api';
import { ProfileComponent } from './pages/profile/profile.component';
import { PlayerComponent } from './pages/player/player.component';

import { EventService } from './services/event.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    SignUpComponent,
    NavbarComponent,
    NewEventComponent,
    NewIncaricatiComponent,
    HomeComponent,
    ProfileComponent,
    PlayerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserModule /* or CommonModule */,
    FormsModule, ReactiveFormsModule,
    CardModule, InputTextModule,
    ButtonModule, MenuModule,
    MenubarModule, DialogModule,
    BrowserAnimationsModule, CalendarModule, DropdownModule, ConfirmDialogModule,
    ToastModule, PasswordModule, ConfirmPopupModule, TooltipModule, MessagesModule,
    MessageModule, RadioButtonModule, InputSwitchModule, InputNumberModule, AccordionModule

  ],
  providers: [
    MessageService,
    LoginService,
    UserService,
    EventService, ConfirmationService, 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
