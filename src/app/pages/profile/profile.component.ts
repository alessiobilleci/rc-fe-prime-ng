import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ChangePasswordDTO } from '../../models/changePassword.model'
import { ErrorService } from 'src/app/services/error.service';
import { LoginService } from 'src/app/services/login.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  user;
  changePassword = new ChangePasswordDTO;
  cambiaPassword: boolean;
  changePasswordForm = new FormGroup({
    currentPassword: new FormControl(''),
    newPassword: new FormControl(''),
    repeatPassword: new FormControl(''),
  })
  errorMsg: any;
  constructor(private userService: UserService, private errorService: ErrorService, private loginService: LoginService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  toggleNewsletter() {
    this.userService.changeNewsletter().subscribe();
  }

  get currentPassword() { return this.changePasswordForm.get('currentPassword'); }
  get newPassword() { return this.changePasswordForm.get('newPassword'); }
  get repeatPassword() { return this.changePasswordForm.get('repeatPassword'); }

  onSubmit() {
    // console.log(this.user);

    // debugger
    this.changePassword.username = this.user.username;
    this.changePassword.currentPassword = this.currentPassword.value;
    // console.log(this.changePassword.currentPassword);
    this.changePassword.newPassword = this.newPassword.value;
    if (this.newPassword.value !== this.repeatPassword.value) {
      this.errorMsg = "Le due password devono coincidere";
    } else {
      this.userService.changePassword(this.changePassword).subscribe((data) => { this.loginService.logout().subscribe() }, (error) => { this.errorMsg = this.errorService.findError(error) });
    }
  }
}
