import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  role = JSON.parse(localStorage.getItem('role'));
  user = JSON.parse(localStorage.getItem('user'));
  token = JSON.parse(localStorage.getItem('token')).token;
  items: MenuItem[];
  display = false;
  displayIncaricati = false;
  displayProfile = false;
  categories = [
    { label: 'Calcio a 5', value: 'Calcio a 5' },
    { label: 'Calcio a 7', value: 'Calcio a 7' },
    { label: 'Calcio a 11', value: 'Calcio a 11' },
    { label: 'Basket', value: 'Basket' },
    { label: 'Pallavolo', value: 'Pallavolo' }];

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    console.log(this.role);

    this.items = [
      {
        visible: this.role === "ADMIN",
        label: 'Gestione Utenti',
        icon: 'pi pi-fw pi-users',
        command: () => this.showIncaricatiDialog()
      },
      {
        visible: this.role === "ADMIN",
        label: 'Aggiungi Evento',
        icon: 'pi pi-plus',
        command: () => this.showDialog()
      }, {
        label: 'Profilo',
        icon: 'pi pi-user',
        command: () => this.showProfileDialog()
      }
    ];
  }
   isAdmin() {
    return this.user.role == "ADMIN";
  }
  showDialog() {
    this.display = true;
  }
  showIncaricatiDialog() {
    this.displayIncaricati = true;
  }
  showProfileDialog() {
    this.displayProfile = true;
  }
  logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('role');
    this.loginService.logout().subscribe();
  }

}
