import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, RouterLink, ActivatedRoute, ParamMap } from '@angular/router';

import { first } from 'rxjs/operators';
import { UserDTO } from '../../models/user';
import { Validators } from '@angular/forms';
import { MessageService } from 'primeng/api'
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loading = false;
  submitted = false;
  user = new UserDTO;
  returnUrl: string;
  errmsg = '';
  errorData: string;
  input: string
  isAdmin: boolean
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  position: string;
  displayPosition: boolean;
  msgs;

  constructor(private loginService: LoginService,
    private router: Router,
    private messageService: MessageService,
    private userService: UserService) { }

  ngOnInit(): void { }

  // easy access to form fields
  get f() { return this.loginForm.controls; }

  get username() { return this.loginForm.get('username'); }
  get password() { return this.loginForm.get('password'); }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.loginService.login(this.username.value, this.password.value)
      .pipe(first())
      .subscribe(
        (success) => {
          this.router.navigate(['home']);
        },
        (error) => {
          this.errorData = 'Username e/o password errati';
          this.messageService.add({severity: 'error', summary: 'Credenziali errate!' });
          this.loginForm.get('password').reset();
        });
  }
  showPositionDialog(position: string) {
    this.position = position;
    this.displayPosition = true;
  }
  recoverPassword(username) {
    this.userService.recoverPassword(username).subscribe(
      (success) => { window.location.reload(); }
    );
  }
}
