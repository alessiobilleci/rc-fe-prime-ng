import { Component, OnInit } from '@angular/core';
import { UserDTO } from '../../models/user';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { ErrorService } from 'src/app/services/error.service';
import {iif} from "rxjs";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html'
})
export class SignUpComponent implements OnInit {
  isValid: boolean;

  registerForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.pattern('^[a-z A-Z]*$')]),
    surname: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    repeatPassword: new FormControl('', Validators.required),
    externalUser: new FormControl(0),
    newsletter: new FormControl(0)
  });

  loading = false;
  submitted = false;
  nameModel = '';
  surnameModel = '';
  errorMsg: any;
  constructor(
    private router: Router,
    private errorService: ErrorService,
    private userService: UserService,
  ) {
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  get name() { return this.registerForm.get('name'); }
  get surname() { return this.registerForm.get('surname'); }
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('password'); }
  get repeatPassword() { return this.registerForm.get('repeatPassword'); }
  get newsletter() { return this.registerForm.get('newsletter'); }
  get externalUser() { return this.registerForm.get('externalUser'); }

  user = new UserDTO();


  ngOnInit(): void {
  }
  onSubmit() {
    const username =
      document.getElementById('username').innerText;
    this.user.username = username;
    this.user.name = this.registerForm.value.name;
    this.user.surname = this.registerForm.value.surname;
    this.user.email = this.registerForm.value.email;
    this.user.password = this.registerForm.value.password;
    this.user.newsletter = this.registerForm.value.newsletter;
    if (this.registerForm.value.externalUser) {
      this.user.role = 'EXTERNAL';
    }
    this.submitted = true;
    // reset alerts on submit
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.userService.register(this.user)
      .subscribe(
        (data) => {
          this.router.navigate(['/login']);
        },
        (error) => {
          this.errorMsg = this.errorService.findError(error);
        });
  }
  goToLogin() {
    this.router.navigate(['login']);
  }
}
