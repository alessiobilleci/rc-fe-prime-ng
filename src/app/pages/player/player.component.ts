import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EventDTO } from 'src/app/models/event';
import { PlayerDTO } from 'src/app/models/player.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html'
})
export class PlayerComponent implements OnInit {
  @Input() iscritto: PlayerDTO;
  @Input() isAdmin: boolean;
  @Input() event: EventDTO;
  @Output() selected: EventEmitter<any> = new EventEmitter<any>();
  @Output() onRemove: EventEmitter<any> = new EventEmitter<any>();
  selectedValue: string;
  isExternal: boolean;
  constructor(private userService : UserService) { }

  ngOnInit(): void {
    if (this.iscritto.role === 'EXTERNAL') {
      this.isExternal = true;
    } else {
      this.isExternal = false;
    }
  }

  assignTeam() {
    let param = {
      username: this.iscritto.username,
      team: this.selectedValue,
    }
    this.selected.emit(param);
  }
  removePlayer(username){
    this.onRemove.emit(username)
  }
}
