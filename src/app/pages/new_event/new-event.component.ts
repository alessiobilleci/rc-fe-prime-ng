import { Component, OnInit } from '@angular/core';
import { EventService } from '../../services/event.service';
import { EventDTO } from '../../models/event';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserDTO } from '../../models/user';
import { AuthGuard } from '../../auth.guard';
import { ErrorService } from '../../services/error.service';
@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.component.html'
})
export class NewEventComponent implements OnInit {
  role = JSON.parse(localStorage.getItem('role'));
  user = new UserDTO;
  event = new EventDTO;
  loading = false;
  submitted = false;
  today = new Date();
  minDateValue = new Date(this.today.getTime() + 24 * 60 * 60 * 1000);
  categories = [];
  constructor(private eventService: EventService, public authGuard: AuthGuard, private errorService: ErrorService) { }

  newEventForm = new FormGroup({
    category: new FormControl('', (Validators.required)),
    dateTime: new FormControl('', (Validators.required)),
    hourOfFreePeriod: new FormControl(24),
    hourOfNoDeleteZone: new FormControl(6),
  });
  ngOnInit(): void {
    if(this.role === 'ADMIN'){
      this.eventService.getCategories().subscribe((data) => {
        this.categories = data.categories;
      }, (error) => { this.errorService.findError(error); });
    }
  }
  onSubmit() {
    this.event.category = this.newEventForm.value.category;
    this.event.date = this.newEventForm.value.dateTime;
    this.event.hourOfFreePeriod = this.newEventForm.value.hourOfFreePeriod;
    this.event.hourOfNoDeleteZone = this.newEventForm.value.hourOfNoDeleteZone;


    this.submitted = true;
    // stop here if form is invalid
    if (this.newEventForm.invalid) {
      return;
    }
    this.loading = true;
    this.eventService.createEvent(this.event).subscribe((data) => { window.location.reload(); }, (error) => { this.errorService.findError(error) });
  }

}
