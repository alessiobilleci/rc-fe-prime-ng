import { Component, OnInit } from '@angular/core';
import { UserDTO } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { EventService } from '../../services/event.service';
import { ConfirmationService } from 'primeng/api'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PlayerDTO } from 'src/app/models/player.model';
import { ErrorService } from '../../services/error.service';
import { EventDTO } from 'src/app/models/event';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [MessageService]
})
export class HomeComponent implements OnInit {
  events;
  errorMsg;
  user: UserDTO;
  isAdmin: boolean;
  displayTeams: boolean;
  iscritti: PlayerDTO[] = [];
  subscribedEvents;
  eventId;
  subscribed: boolean;
  selectTeamsForm: FormGroup;
  teams = [];
  riserve = [];
  isDisabled: boolean = true;
  showEditEvent: boolean;
  paymentModal: boolean;
  event;
  edit;
  username;
  minDateValue;
  eventToPass;
  today = new Date;
  confirmationModal;
  freePeriodDescription = 'Numero di ore prima dell\'evento oltre il quale chiunque può iscriversi; anche chi è iscritto ad altri eventi.';
  noDeleteZoneDesc = 'Numero di ore prima dell\'evento oltre il quale non si può annullare la propria iscrizione.';
  categories = [];
  modelOperation: string;
  constructor(private errorService: ErrorService, private router: Router, private eventService: EventService, private userService: UserService, private confirmationService: ConfirmationService, private fb: FormBuilder, private messageService: MessageService) { }

  editEventForm = new FormGroup({
    category: new FormControl('', (Validators.required)),
    dateTime: new FormControl('', (Validators.required)),
    hourOfFreePeriod: new FormControl(24),
    hourOfNoDeleteZone: new FormControl(6),
  });
  ngOnInit(): void {
    this.riserve = [];
    this.userService.findSubscribed().subscribe((result) => {
      this.subscribedEvents = result;
      this.errorMsg = null;
      if (this.subscribedEvents.length > 0) {
        this.subscribed = true;
      }
    }, (error) => {
      this.errorService.findError(error);
    });
    this.userService.getUser().subscribe(data => {
      this.user = data;
      this.isAdmin = this.user.role === 'ADMIN';
    }, (error) => {
      this.errorService.findError(error);
    });

    this.eventService.getEvents().subscribe(data => {
      this.events = data;
    }, (error) => {
      this.errorMsg = this.errorService.findError(error);
    });
    if (this.isAdmin) {
      this.fillCategories();
    }
  }

  findPlayers(id, page) {
    this.errorMsg = null;
    this.eventService.findPlayers(id, page).subscribe(data => {
      if (page == 0) {
        this.teams = [];
        this.iscritti = data;
        this.iscritti.forEach(element => {
          let obj = {
            username: element.username, team: element.team
          };

          if (element.team != null) {
            this.teams.push(obj);
          }
        });
      } else if (page == 1) {
        this.riserve = data;
      }
      this.displayTeams = true;
      this.eventId = id;
    }, (error) => {
      this.errorService.findError(error);
    });
  }

  populateArray(username, team) {
    if (this.teams.length > 0) {
      this.isDisabled = false;
    } else {
      this.isDisabled = true;
    }
    let obj = {
      username: username, team: team
    };
    let toAdd = true;


    this.teams.forEach(element => {

      if (element.username === username) {
        element.team = team;
        toAdd = false;
      }
    });

    if (toAdd) {
      this.teams.push(obj)
    }
  }

  defineTeams() {
    let whites: PlayerDTO[] = [];
    let blacks: PlayerDTO[] = [];
    this.teams.forEach(element => {

      if (element.team === 'WHITE') {
        whites.push(element.username)
      } else {
        blacks.push(element.username)
      }
    });
    this.eventService.setTeam(this.eventId, blacks, whites).subscribe(
      (data) => {
        window.location.reload();
      }, (error) => {
        this.showError(this.errorService.findError(error));
      }
    );
    whites = []
    blacks = []
  }


  confirm(id, userRole) {
    this.eventId = id;
    this.modelOperation = 'booking';
    if (userRole === 'EXTERNAL') {
      this.paymentModal = true;
    } else {
      this.confirmationModal = true;
    }
  }

  confirmIscrizione() {
    if (this.modelOperation === 'booking') {
      this.userService.bindWithEvent(this.eventId).subscribe((result) => {
        this.ngOnInit();
        this.showSuccess('Iscrizione');
      }, (error) => {
        this.showError(this.errorService.findError(error));
      });
    } else {
      this.userService.removeFromEvent(this.eventId, this.username).subscribe(data => {
        this.ngOnInit();
        this.showSuccess('Cancellazione');
      }, (error) => {
        this.showError(this.errorService.findError(error));
      });
    }
    this.confirmationModal = false;
    this.paymentModal = false;
  }
  confirmAnnullaIscrizione(id, username: string) {
    this.eventId = id;
    this.username = username;
    this.modelOperation = 'notBooking';
    this.showEditEvent = false;
    this.confirmationModal = true;
  }

  confirmDeleteEvent(eventId) {
    this.confirmationModal = true;
    this.eventId = eventId;
    this.modelOperation = 'canceling';
  }

  deleteEvent() {
    this.eventService.deleteEvent(this.eventId).subscribe(data => {
      this.confirmationModal = false;
      this.ngOnInit();
    }, error => {
      this.errorService.findError(error);
    });
  }
  editEvent(event: EventDTO) {
    if (this.categories.length === 0) {
      this.fillCategories();
    }
    this.eventToPass = event;
    console.log(this.showEditEvent);
    this.editEventForm.controls.category.setValue(event.category);
    this.editEventForm.controls.dateTime.setValue(new Date(event.hiddenTime));
    this.editEventForm.controls.hourOfFreePeriod.setValue(event.hourOfFreePeriod);
    this.editEventForm.controls.hourOfNoDeleteZone.setValue(event.hourOfNoDeleteZone);
    this.showEditEvent = true;
    console.log(this.showEditEvent);
  }
  removePlayer(username) {
    this.userService.removeFromEvent(this.eventId, username).subscribe(data => {
      this.findPlayers(this.eventId, 0);
      this.findPlayers(this.eventId, 1);
    });
  }

  onSubmit() {
    this.eventToPass.category = this.editEventForm.value.category;
    this.eventToPass.date = this.editEventForm.value.dateTime;
    this.eventToPass.hourOfFreePeriod = this.editEventForm.value.hourOfFreePeriod;
    this.eventToPass.hourOfNoDeleteZone = this.editEventForm.value.hourOfNoDeleteZone;
    this.eventService.editEvent(this.eventToPass).subscribe(data => {
      this.showEditEvent = false;
      window.location.reload();
    });
  }
  fillCategories() {
    this.eventService.getCategories().subscribe((data) => {
      this.categories = data.categories;
    }, (error) => { this.errorService.findError(error); });
  }

  showError(summary) {
    this.messageService.add({severity: 'error', summary});
  }
  showSuccess(summary) {
    this.messageService.add({severity: 'success', summary: summary + ' completata'});
  }

  payPalRedirect() {
    window.open('https://www.google.it');
  }
}
