import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

import {ConfirmationService, MessageService} from 'primeng/api';
import { ErrorService } from '../../services/error.service';
import { Router } from '@angular/router';
import {Clipboard} from '@angular/cdk/clipboard';
@Component({
  selector: 'app-new-incaricati',
  templateUrl: './new-incaricati.component.html'
})
export class NewIncaricatiComponent implements OnInit {
  users;
  loggedUser = JSON.parse(localStorage.getItem('user'));
  errorMsg: any;
  confirmationModal: boolean;
  username;
  constructor(private confirmationService: ConfirmationService, private userService: UserService, private router: Router, private errorService: ErrorService, private clipBoard: Clipboard, private messageService: MessageService) { }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe((resp) => {
      this.users = resp;
    }, (error) => {
      this.errorMsg = this.errorService.findError(error);
    });
  }
  changeRole(username, role) {
    // tslint:disable-next-line:max-line-length
    this.userService.changeRole(username, role).subscribe((data) => { this.ngOnInit(); }, (error) => { this.errorMsg = this.errorService.findError(error); });
  }
  copyEmail(email) {
    this.clipBoard.copy(email);
    this.messageService.add({severity: 'success', summary: 'Email copiata'});
  }
  delete() {
    // tslint:disable-next-line:max-line-length
    this.confirmationModal = false;
    this.userService.delete(this.username).subscribe((data) => { this.ngOnInit(); }, (error) => { this.errorMsg = this.errorService.findError(error); });
  }

  confirmDelete(username) {
    this.confirmationModal = true;
    this.username = username;
  }

}
